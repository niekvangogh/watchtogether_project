namespace Watchtogether.DTO
{
    public class PlaylistCreationDto
    {
        public string Name { get; set; }

        public int OwnerId { get; set; }

        public int CategoryId { get; set; }
    }
}