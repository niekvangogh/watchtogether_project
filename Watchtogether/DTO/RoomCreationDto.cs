using Watchtogether.Model.Models;

namespace Watchtogether.DTO
{
    public class RoomCreationDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Public { get; set; }

        public User Owner { get; set; }

    }
}