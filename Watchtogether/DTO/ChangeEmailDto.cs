namespace Watchtogether.DTO
{
    public class ChangeEmailDto
    {
        public string OldEmail { get; set; }

        public string NewEmail { get; set; }
    }
}