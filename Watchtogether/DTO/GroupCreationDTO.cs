namespace Watchtogether.DTO
{
    public class GroupCreationDTO
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}