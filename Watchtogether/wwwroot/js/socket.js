"use strict";


const connection = new signalR.HubConnectionBuilder().withUrl("/socket").build();

$(document).ready(function () {
    connection.start().then(function () {
        connection.invoke("Authenticate", roomId)
    }).catch(function (error) {
        console.log(error);
    });

    connection.on("Authenticated", function (data) {

    });

    connection.on("QueueVideo", function (data) {
        setCurrentVideo(data.videoUrl, data.timestamp);
    });

    connection.on("SetVideoStatus", function (data) {

        if (data.status === 1) {
            player.playVideo();
        } else if (data.status === 2) {
            player.pauseVideo();
        }
    });

    connection.on("SetVideoTime", function (data) {
        player.seekTo(data.time, true);
    })
});

function playVideo(url) {
    connection.invoke("QueueVideo", roomId, url);
}