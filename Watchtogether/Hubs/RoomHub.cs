using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Watchtogether.Factory;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Models;
using Watchtogether.Utils;
using Watchtogether.ViewModels.Socket;

namespace Watchtogether.Hubs
{
    public class RoomHub : Hub
    {
        private readonly IRoomLogic _roomLogic;
        private readonly IUserLogic _userLogic;


        public RoomHub(Factory.Factory factory)
        {
            _userLogic = factory.GetUserLogic(Engine.MySql);
            _roomLogic = factory.GetRoomLogic(Engine.MySql);
        }


        public async Task Authenticate(int roomId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, roomId.ToString());
            await Clients.Caller.SendAsync("Authenticated", "test");
        }

        public async Task QueueVideo(int roomId, string videoUrl)
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(Context));
            if (_roomLogic.Read(roomId).OwnerId == user.Id)
            {
                await Clients.Group(roomId.ToString()).SendAsync("QueueVideo", new QueueVideoModel
                {
                    VideoUrl = videoUrl,
                    UserName = user.Username
                });
            }
        }

        public async Task SetVideoStatus(int roomId, int status)
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(Context));
            if (_roomLogic.Read(roomId).OwnerId == user.Id)
            {
                await Clients.Group(roomId.ToString()).SendAsync("SetVideoStatus", new VideStatusChangeModel
                {
                    Status = status
                });
            }
        }

        public async Task SetVideoTime(int roomId, int time)
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(Context));
            if (_roomLogic.Read(roomId).OwnerId == user.Id)
            {
                await Clients.OthersInGroup(roomId.ToString()).SendAsync("SetVideoTime", new VideoTimeChangeModel
                {
                    Time = time
                });
            }
        }
    }
}