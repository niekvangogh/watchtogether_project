namespace Watchtogether.ViewModels.Socket
{
    public class QueueVideoModel
    {
        public string UserName { get; set; }
        
        public string VideoUrl { get; set; }
    }
}