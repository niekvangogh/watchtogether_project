namespace Watchtogether.ViewModels.Socket
{
    public class VideStatusChangeModel
    {
        public int Status { get; set; }
    }
}