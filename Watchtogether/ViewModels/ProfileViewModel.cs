using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.ViewModels
{
    public class ProfileViewModel
    {
        public IEnumerable<Playlist> Playlists { get; set; }

        public IEnumerable<Room> Rooms { get; set; }
        
        public IEnumerable<Group> Groups { get; set; }
    }
}