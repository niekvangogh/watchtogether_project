using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.ViewModels
{
    public class PlaylistViewModel
    {
        public Playlist Playlist { get; set; }

        public IEnumerable<PlaylistItem> PlaylistItems { get; set; }
    }
}