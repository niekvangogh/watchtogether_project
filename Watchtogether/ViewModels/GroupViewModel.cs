using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.ViewModels
{
    public class GroupViewModel
    {
        public Group Group { get; set; }

        public IEnumerable<User> Users { get; set; }
    }
}