using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.ViewModels
{
    public class RoomViewModel
    {
        public Room Room { get; set; }
        
        public IEnumerable<Group> UserGroups { get; set; }
        
        public IEnumerable<Group> CurrentGroups { get; set; }
    }
}