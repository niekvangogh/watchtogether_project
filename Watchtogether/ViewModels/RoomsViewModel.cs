using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.ViewModels
{
    public class RoomsViewModel
    {
        public IEnumerable<Room> Rooms { get; set; }
    }
}