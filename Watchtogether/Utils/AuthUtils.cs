using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;

namespace Watchtogether.Utils
{
    public static class AuthUtils
    {
        public static async Task SignInUser(HttpContext context, string email)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, email)
            };
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme, "email", null);
            var principal = new ClaimsPrincipal(identity);

            await context.SignInAsync(principal);
        }

        public static string GetCurrentUserMail(HttpContext httpContext)
        {
            var claims = httpContext.User.Claims.ToList();
            return claims.First(claim => claim.Subject.NameClaimType == "email").Value;
        }
        
        public static string GetCurrentUserMail(HubCallerContext context)
        {
            var claims = context.User.Claims.ToList();
            return claims.First(claim => claim.Subject.NameClaimType == "email").Value;
        }

        public static string AuthenticateWebsocketSession(HttpContext context)
        {
            return GetCurrentUserMail(context);
        }
    }
}