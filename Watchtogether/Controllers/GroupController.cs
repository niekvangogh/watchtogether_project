using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Watchtogether.DTO;
using Watchtogether.Exceptions;
using Watchtogether.Factory;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Utils;
using Watchtogether.ViewModels;

namespace Watchtogether.Controllers
{
    [Route("groups")]
    public class GroupController : Controller
    {
        private readonly IGroupLogic _groupLogic;
        private readonly IUserLogic _userLogic;

        public GroupController(IConfiguration config, IHostingEnvironment env, Factory.Factory factory)
        {
            _groupLogic = factory.GetGroupLogic(Engine.MySql);
            _userLogic = factory.GetUserLogic(Engine.MySql);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Index()
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(HttpContext));
            ViewBag.Groups = _groupLogic.GetUserGroups(user);
            return View();
        }

        [Authorize]
        [HttpGet("{id}")]
        public IActionResult OpenGroup(int id)
        {
            var model = new GroupViewModel {Group = _groupLogic.Read(id), Users = _groupLogic.GetUsersFromGroup(id)};
            return View(model);
        }

        [Authorize]
        [HttpGet("edit/{id}")]
        public IActionResult Edit(int id)
        {
            ViewBag.Group = _groupLogic.Read(id);
            return View();
        }

        [Authorize]
        [HttpPost("create")]
        public IActionResult CreateGroup(GroupCreationDTO dto)
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail((HttpContext)));
            var group = _groupLogic.Create(new Group
            {
                Name = dto.Name,
                Description = dto.Description,
                OwnerId = user.Id,
                CreatorId = user.Id,
                CreationDate = DateTime.Now
            });

            _groupLogic.AddUser(group, user);
            return RedirectToAction("OpenGroup", new {id = group.Id});
        }

        [Authorize]
        [HttpGet("create")]
        public IActionResult Create()
        {
            return View();
        }
        
        [Authorize]
        [HttpPost("{groupId}/add/{username}")]
        public IActionResult AddUser(int groupId, string username)
        {
            var group = _groupLogic.Read(groupId);
            try
            {
                var user = _userLogic.FindByUsername(username);
                _groupLogic.AddUser(group, user);
            }
            catch (WatchtogetherException e)
            {
                return Content(e.Message);
            }

            return Ok("User was added");
        }

        [Authorize]
        [HttpPost("{groupId}/remove/{username}")]
        public IActionResult RemoveUser(int groupId, string username)
        {
            var group = _groupLogic.Read(groupId);

            try
            {
                var user = _userLogic.FindByUsername(username);
                _groupLogic.RemoveUser(group, user);
            }
            catch (WatchtogetherException e)
            {
                return Content(e.Message);
            }

            return Ok("User was removed");
        }
    }
}