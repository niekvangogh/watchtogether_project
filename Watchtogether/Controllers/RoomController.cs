using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Watchtogether.DTO;
using Watchtogether.Exceptions;
using Watchtogether.Factory;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Utils;
using Watchtogether.ViewModels;

namespace Watchtogether.Controllers
{
    [Route("rooms")]
    public class RoomController : Controller
    {
        private readonly IRoomLogic _roomLogic;
        private readonly IUserLogic _userLogic;
        private readonly IGroupLogic _groupLogic;

        public RoomController(IConfiguration config, IHostingEnvironment env, Factory.Factory factory)
        {
            _roomLogic = factory.GetRoomLogic(Engine.MySql);
            _userLogic = factory.GetUserLogic(Engine.MySql);
            _groupLogic = factory.GetGroupLogic(Engine.MySql);
        }

        [Authorize]
        public IActionResult Index()
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(HttpContext));
            var rooms = _roomLogic.GetAllAccessibleRooms(user);

            return View(new RoomsViewModel
            {
                Rooms = rooms
            });
        }


        [Authorize]
        [Route("{id}")]
        public IActionResult OpenRoom(int id)
        {
            var room = _roomLogic.Read(id);
            if (room == null)
            {
                return NotFound("Room not found");
            }

            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(HttpContext));
            var userGroups = _groupLogic.GetUserGroups(user);
            var currentGroups = _roomLogic.GetGroups(room);


            var viewmodel = new RoomViewModel
            {
                Room = room,
                UserGroups = userGroups.Where(group => currentGroups.All(currentGroup => currentGroup.Id != @group.Id)),
                CurrentGroups = currentGroups,
            };
            return View(viewmodel);
        }


        [HttpGet]
        [Route("create")]
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Route("create")]
        [Authorize]
        public ActionResult CreateRoom(RoomCreationDto dto)
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(HttpContext));

            var room = new Room
            {
                Name = dto.Name,
                Public = dto.Public,
                Description = dto.Description,
                CreationDate = DateTime.Now,
                OwnerId = user.Id,
                CreatorId = user.Id
            };

            room = _roomLogic.Create(room);
            return RedirectToAction("OpenRoom", new {id = room.Id});
        }

        [HttpGet]
        [Route("{id}/edit")]
        [Authorize]
        public IActionResult Edit(int id)
        {
            var room = _roomLogic.Read(id);
            return View(room);
        }

        [HttpPost]
        [Route("{id}/edit")]
        [Authorize]
        public IActionResult EditRoom(RoomCreationDto dto, int id)
        {
            var room = new Room
            {
                Id = id,
                Name = dto.Name,
                Public = dto.Public,
                Description = dto.Description
            };

            room = _roomLogic.Update(room);
            return RedirectToAction("OpenRoom", new {id});
        }

        [HttpPost]
        [Route("{roomId}/group/add/{groupId}")]
        [Authorize]
        public IActionResult AddGroup(int roomId, int groupId)
        {
            var room = _roomLogic.Read(roomId);
            var group = _groupLogic.Read(groupId);

            try
            {
                _roomLogic.AddGroup(room, group);
            }
            catch (WatchtogetherException e)
            {
                return Ok(e.Message);
            }

            return Ok("The group was added to the room");
        }

        [HttpPost]
        [Route("{roomId}/group/remove/{groupId}")]
        [Authorize]
        public IActionResult RemoveGroup(int roomId, int groupId)
        {
            var room = _roomLogic.Read(roomId);
            var group = _groupLogic.Read(groupId);

            try
            {
                _roomLogic.RemoveGroup(room, group);
            }
            catch (WatchtogetherException e)
            {
                return Ok(e.Message);
            }

            return Ok("The group was removed from the room");
        }
    }
}