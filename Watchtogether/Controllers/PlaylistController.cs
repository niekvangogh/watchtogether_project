using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Watchtogether.DTO;
using Watchtogether.Factory;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Utils;
using Watchtogether.ViewModels;

namespace Watchtogether.Controllers
{
    [Route("playlists")]
    public class PlaylistController : Controller
    {
        private readonly IPlaylistLogic _playlistLogic;
        private readonly IUserLogic _userLogic;

        public PlaylistController(IConfiguration config, IHostingEnvironment env, Factory.Factory factory)
        {
            _playlistLogic = factory.GetPlaylistLogic(Engine.MySql);
            _userLogic = factory.GetUserLogic(Engine.MySql);
        }

        [Route("{id}")]
        public IActionResult OpenPlaylist(int id)
        {
            var playlist = _playlistLogic.Read(id);
            if (playlist == null)
            {
                return NotFound("Room not found");
            }

            var playlistItems = _playlistLogic.GetPlaylistItems(playlist);

            var viewmodel = new PlaylistViewModel();
            viewmodel.Playlist = playlist;
            viewmodel.PlaylistItems = playlistItems;
            return View(viewmodel);
        }


        [HttpGet]
        [Route("create")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Route("create")]
        public ActionResult CreatePlaylist(PlaylistCreationDto dto)
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(HttpContext));

            var playlist = new Playlist
            {
                OwnerId = user.Id
            };

            playlist = _playlistLogic.Create(playlist);
            return RedirectToAction("OpenPlaylist", new {id = playlist.Id});
        }

        [HttpGet]
        [Route("edit/{id}")]
        public IActionResult Edit(int id)
        {
            var playlist = _playlistLogic.Read(id);
            return View(playlist);
        }

        [HttpPost]
        [Route("edit/{id}")]
        public IActionResult EditPlaylist(PlaylistCreationDto dto, int id)
        {
            var playlist = new Playlist
            {
                Id = id,
                Name = dto.Name,
                OwnerId = dto.OwnerId,
                CategoryId = dto.CategoryId
            };

            playlist = _playlistLogic.Update(playlist);
            return RedirectToAction("OpenPlaylist", new {id});
        }
    }
}