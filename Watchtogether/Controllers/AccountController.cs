using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Watchtogether.DTO;
using Watchtogether.Factory;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Utils;
using Watchtogether.ViewModels;

namespace Watchtogether.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserLogic _userLogic;
        private readonly IRoomLogic _roomLogic;
        private readonly IGroupLogic _groupLogic;
        private readonly IPlaylistLogic _playlistLogic;

        public AccountController(IConfiguration config, IHostingEnvironment env, Factory.Factory factory)
        {
            _userLogic = factory.GetUserLogic(Engine.MySql);
            _groupLogic = factory.GetGroupLogic(Engine.MySql);
            _roomLogic = factory.GetRoomLogic(Engine.MySql);
            _playlistLogic = factory.GetPlaylistLogic(Engine.MySql);
        }

        [Authorize]
        [Route("profile")]
        [HttpGet]
        public IActionResult Index()
        {
            var user = _userLogic.FindByEmail(AuthUtils.GetCurrentUserMail(HttpContext));
            var groups = _groupLogic.GetUserGroups(user);
            var rooms = _roomLogic.GetRoomsFromUser(user);
            var playlists = _playlistLogic.GetPlaylistsForUser(user);

            var model = new ProfileViewModel
            {
                Rooms = rooms,
                Playlists = playlists,
                Groups = groups
            };

            return View(model);
        }

        [Authorize]
        [Route("settings")]
        [HttpGet]
        public IActionResult Settings()
        {
            return View();
        }

        [Authorize]
        [Route("settings/update/password")]
        [HttpGet]
        public IActionResult UpdatePassword()
        {
            return View();
        }

        [Authorize]
        [Route("settings/update/email")]
        [HttpGet]
        public IActionResult UpdateEmail()
        {
            return View();
        }

        [Authorize]
        [Route("settings/update/email")]
        [HttpPost]
        public IActionResult UpdateEmail(ChangeEmailDto dto)
        {
            _userLogic.ChangeEmail(dto.OldEmail, dto.NewEmail);
            return RedirectToAction("Index");
        }
    }
}