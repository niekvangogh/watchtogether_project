using System;
using System.Threading.Tasks;
using DevOne.Security.Cryptography.BCrypt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Watchtogether.DTO;
using Watchtogether.Factory;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Utils;

namespace Watchtogether.Controllers
{
    [Route("auth")]
    public class AuthController : Controller
    {
        private readonly IUserLogic _userLogic;

        public AuthController(IConfiguration config, IHostingEnvironment env, Factory.Factory factory)
        {
            _userLogic = factory.GetUserLogic(Engine.MySql);
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginDto login)
        {
            User user;
            try
            {
                user = _userLogic.Login(login.Email, login.Password);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return RedirectToAction("Login", "Home");
            }

            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }

            await AuthUtils.SignInUser(HttpContext, user.Email);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register(RegisterDto dto)
        {
            try
            {
                _userLogic.Register(dto.Username, dto.Password, dto.Email, dto.Birthday);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return RedirectToAction("Register", "Home");
            }

            await AuthUtils.SignInUser(HttpContext, dto.Email);
            return RedirectToAction("Index", "Account");
        }

        [HttpGet]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }
    }
}