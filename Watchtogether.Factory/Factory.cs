﻿using System;
using System.Collections.Generic;
using Watchtogether.DAL.Memory;
using Watchtogether.DAL.MySql;
using Watchtogether.Logic;
using Watchtogether.Logic.Interface;
using Watchtogether.Repository;

namespace Watchtogether.Factory
{
    public class Factory
    {
        private readonly Dictionary<Engine, string> _connectionStrings;

        public Factory(Dictionary<Engine, string> connectionStrings)
        {
            _connectionStrings = connectionStrings;
            foreach (var (key, value) in _connectionStrings)
            {
            }
        }

        public IUserLogic GetUserLogic(Engine engine)
        {
            switch (engine)
            {
                case Engine.MySql:
                    return new UserLogic(new UserRepository(new UserMySqlContext(_connectionStrings[engine])));
                default:
                    return new UserLogic(new UserRepository(new UserMemoryContext()));
            }
        }

        public IRoomLogic GetRoomLogic(Engine engine)
        {
            switch (engine)
            {
                case Engine.MySql:
                    return new RoomLogic(new RoomRepository(new RoomMySqlContext(_connectionStrings[engine])));
                default:
                    throw new NotImplementedException();
            }
        }

        public IPlaylistLogic GetPlaylistLogic(Engine engine)
        {
            switch (engine)
            {
                case Engine.MySql:
                    return new PlaylistLogic(
                        new PlaylistRepository(new PlaylistMySqlContext(_connectionStrings[engine])));
                default:
                    throw new ArgumentOutOfRangeException(nameof(engine), engine, null);
            }
        }

        public IGroupLogic GetGroupLogic(Engine engine)
        {
            switch (engine)
            {
                case Engine.MySql:
                    return new GroupLogic(
                        new GroupRepository(new GroupMySqlContext(_connectionStrings[engine])));
                case Engine.Memory:
                    return new GroupLogic(
                        new GroupRepository(new GroupMemoryContext()));
                default:
                    throw new ArgumentOutOfRangeException(nameof(engine), engine, null);
            }
        }
    }
}