using System.Collections.Generic;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository.Interface;

namespace Watchtogether.Repository
{
    public class PlaylistRepository : IPlaylistRepository
    {
        private readonly IPlaylistContext _context;

        public PlaylistRepository(IPlaylistContext context)
        {
            _context = context;
        }

        public IEnumerable<Playlist> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Playlist Create(Playlist entity)
        {
            return _context.Create(entity);
        }

        public Playlist FindById(int id)
        {
            return _context.Read(id);
        }

        public Playlist Update(Playlist entity)
        {
            return _context.Update(entity);
        }

        public bool Delete(int id)
        {
            return _context.Delete(id);
        }

        public IEnumerable<Playlist> GetPlaylistsForUser(User user)
        {
            return _context.GetPlaylistsForUser(user);
        }

        public IEnumerable<PlaylistItem> GetPlaylistItems(Playlist playlist)
        {
            return _context.GetPlaylistItems(playlist);
        }
    }
}