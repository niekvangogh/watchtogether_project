using System.Collections.Generic;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository.Interface;

namespace Watchtogether.Repository
{
    public class GroupRepository : IGroupRepository
    {
        private readonly IGroupContext _context;

        public GroupRepository(IGroupContext context)
        {
            _context = context;
        }

        public IEnumerable<Group> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Group Create(Group entity)
        {
            return _context.Create(entity);
        }

        public Group FindById(int id)
        {
            return _context.Read(id);
        }

        public Group Update(Group entity)
        {
            return _context.Update(entity);
        }

        public bool Delete(int id)
        {
            return _context.Delete(id);
        }

        public IEnumerable<Group> GetGroupsOfUser(User user)
        {
            return _context.GetGroupsOfUser(user);
        }

        public IEnumerable<Group> GetUserGroups(User user)
        {
            return _context.GetUserGroups(user);
        }

        public IEnumerable<User> GetUsersFromGroup(int id)
        {
            return _context.GetUsersFromGroup(id);
        }

        public bool AddUser(Group group, User user)
        {
            return _context.AddUser(group, user);
        }

        public bool RemoveUser(Group @group, User user)
        {
            return _context.RemoveUser(group, user);
        }
    }
}