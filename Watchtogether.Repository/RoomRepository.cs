using System;
using System.Collections.Generic;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository.Interface;

namespace Watchtogether.Repository
{
    public class RoomRepository : IRoomRepository
    {
        private readonly IRoomContext _context;

        public RoomRepository(IRoomContext context)
        {
            _context = context;
        }

        public IEnumerable<Room> GetAll()
        {
            throw new NotImplementedException();
        }

        public Room Create(Room entity)
        {
            return _context.Create(entity);
        }

        public Room FindById(int id)
        {
            return _context.Read(id);
        }

        public Room Update(Room entity)
        {
            return _context.Update(entity);
        }

        public bool Delete(int id)
        {
            return _context.Delete(id);
        }
        
        public IEnumerable<Room> GetAccessibleRoomsForGroup(Group group)
        {
            return _context.GetAccessibleRoomsForGroup(group);
        }

        public IEnumerable<Room> GetRoomsFromUser(User user)
        {
            return _context.GetRoomsFromUser(user);
        }

        public void AddGroup(Room room, Group @group)
        {
            _context.AddGroup(room, group);
        }

        public void RemoveGroup(Room room, Group @group)
        {
            _context.RemoveGroup(room, group);
        }

        public IEnumerable<Group> GetGroups(Room room)
        {
            return _context.GetGroups(room);
        }

        public IEnumerable<Room> GetAllAccessibleRooms(User user)
        {
            return _context.GetAllAccessibleRooms(user);
        }
    }
}