using System;
using System.Collections.Generic;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository.Interface;

namespace Watchtogether.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IUserContext _context;

        public UserRepository(IUserContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetAll()
        {
            return null;
        }

        public User Create(User entity)
        {
            return _context.Create(entity);
        }

        public User FindById(int id)
        {
            return _context.Read(id);
        }

        public User Update(User entity)
        {
            return _context.Update(entity);
        }

        public bool Delete(int id)
        {
            return _context.Delete(id);
        }

        public User FindByEmail(string email)
        {
            return _context.FindByEmail(email);
        }

        public User FindByUsername(string username)
        {
            return _context.FindByUsername(username);
        }
    }
}