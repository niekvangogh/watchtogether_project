using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.Repository.Interface
{
    public interface IPlaylistRepository : IRepository<Playlist>
    {
        IEnumerable<Playlist> GetPlaylistsForUser(User user);
        IEnumerable<PlaylistItem> GetPlaylistItems(Playlist playlist);
    }
}