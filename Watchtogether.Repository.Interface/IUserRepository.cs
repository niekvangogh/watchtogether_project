using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.Repository.Interface
{
    public interface IUserRepository : IRepository<User>
    {
        User FindByEmail(string email);

        User FindByUsername(string username);
    }
}