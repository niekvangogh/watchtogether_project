using System.Collections.Generic;

namespace Watchtogether.Repository.Interface
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();

        T Create(T entity);

        T FindById(int id);

        T Update(T entity);

        bool Delete(int id);
    }
}