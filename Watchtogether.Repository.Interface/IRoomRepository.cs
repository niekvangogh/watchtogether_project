using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.Repository.Interface
{
    public interface IRoomRepository : IRepository<Room>
    {
        IEnumerable<Room> GetAccessibleRoomsForGroup(Group group);

        IEnumerable<Room> GetRoomsFromUser(User user);
    }
}