using System.Collections.Generic;
using System.Linq;
using Watchtogether.DAL.Interface;
using Watchtogether.DAL.MySql.Utils;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.MySql
{
    public class RoomMySqlContext : MySqlContext<Room>, IRoomContext
    {
        public RoomMySqlContext(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Room> GetRoomsFromUser(User user)
        {
            var results = Database.Query("SELECT * FROM `room` WHERE ownerId = ?",
                user.Id).ToList();
            foreach (var result in results)
            {
                yield return ModelCreator<Room>.CreateModel(result);
            }
        }

        public IEnumerable<Room> GetAccessibleRoomsForGroup(Group group)
        {
            var results =
                Database.Query(
                    "select * from `room` where id = (select roomId from `group_room` where groupId = ?)",
                    group.Id);

            foreach (var result in results)
            {
                yield return ModelCreator<Room>.CreateModel(result);
            }
        }

        public void AddGroup(Room room, Group @group)
        {
            Database.NonQuery("INSERT INTO group_room (groupId, roomId) VALUES(?, ?)", group.Id, room.Id);
        }

        public void RemoveGroup(Room room, Group @group)
        {
            Database.NonQuery("DELETE FROM group_room WHERE groupId = ? AND roomId = ?", group.Id, room.Id);
        }

        public IEnumerable<Group> GetGroups(Room room)
        {
            var results =
                Database.Query(
                    "select `group`.* from `group` INNER JOIN group_room gr on `group`.id = gr.groupId where gr.roomId = ?",
                    room.Id);

            foreach (var result in results)
            {
                yield return ModelCreator<Group>.CreateModel(result);
            }
        }

        public IEnumerable<Room> GetAllAccessibleRooms(User user)
        {
            var results =
                Database.Query(
                    "select `room`.* from `room` INNER JOIN group_room gr on room.id = gr.roomId INNER JOIN user_group ug on ug.userId = ? GROUP BY room.id",
                    user.Id);

            foreach (var result in results)
            {
                yield return ModelCreator<Room>.CreateModel(result);
            }
        }
    }
}