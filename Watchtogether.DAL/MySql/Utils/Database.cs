using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace Watchtogether.DAL.MySql.Utils
{
    public class Database
    {
        private readonly MySqlConnection _connection;

        public Database(string connectionString)    
        {
            _connection = new MySqlConnection(connectionString);
        }

        public IEnumerable<QueryResult> Query(string query, params object[] parameters)
        {
            var results = new List<QueryResult>();

            var command = new MySqlCommand(query, _connection);
            for (var i = 0; i < parameters.Length; i++)
            {
                var parameter = parameters[i];
                command.Parameters.AddWithValue($"param{i}", parameter);
            }

            OpenConnection();
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                results.Add(new QueryResult(reader));
            }

            CloseConnection();

            return results;
        }

        public int NonQuery(string query, params object[] parameters)
        {
            var command = new MySqlCommand(query, _connection);
            for (var i = 0; i < parameters.Length; i++)
            {
                var parameter = parameters[i];
                command.Parameters.AddWithValue($"param{i}", parameter);
            }

            command.CommandText += ";SELECT LAST_INSERT_ID();";
            OpenConnection();
            var id = Convert.ToInt32(command.ExecuteScalar());
            CloseConnection();

            return id;
        }


        private void OpenConnection()
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }
        }

        private void CloseConnection()
        {
            if (_connection.State != ConnectionState.Closed)
            {
                _connection.Close();
            }
        }
    }
}