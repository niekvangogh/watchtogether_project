using System.Collections.Generic;
using System.Linq;
using Watchtogether.DAL.Interface;
using Watchtogether.DAL.MySql.Utils;
using Watchtogether.Model;

namespace Watchtogether.DAL.MySql
{
    public abstract class MySqlContext<T> : IContext<T> where T : DbModel
    {
        protected Database Database { get; }

        protected MySqlContext(string connectionString)
        {
            Database = new Database(connectionString);
        }

        public T Create(T entity)
        {
            var properties = new Dictionary<string, object>();
            var placeholders = new List<string>();
            foreach (var property in typeof(T).GetProperties())
            {
                properties.Add(property.Name, property.GetValue(entity));
                placeholders.Add("?");
            }

            entity.Id = Database
                .NonQuery(
                    $"INSERT INTO `{typeof(T).Name.ToLower()}` ({string.Join(", ", properties.Keys)}) VALUES({string.Join(", ", placeholders)})",
                    properties.Values.ToArray());

            return entity;
        }

        public T Read(int id)
        {
            var results = Database.Query($"SELECT * FROM `{typeof(T).Name.ToLower()}` WHERE id = ?",
                id.ToString()).ToList();
            if (!results.Any())
            {
                return null;
            }

            var result = results.First();
            return ModelCreator<T>.CreateModel(result);
        }

        public T Update(T entity)
        {
            var properties = new Dictionary<string, object>();
            var placeholders = new List<string>();
            foreach (var property in typeof(T).GetProperties())
            {
                properties.Add(property.Name, property.GetValue(entity));
                placeholders.Add("?");
            }

            entity.Id = Database.NonQuery(
                $"UPDATE `{typeof(T).Name.ToLower()}` SET {string.Join(" = ?", properties.Keys)})",
                properties.Values.ToArray());


            return entity;
        }

        public bool Delete(int id)
        {
            Database.Query($"DELETE FROM `{typeof(T).Name.ToLower()}` WHERE id = ?", id.ToString());
            return true;
        }
    }
}