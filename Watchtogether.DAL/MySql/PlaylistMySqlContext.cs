using System.Collections.Generic;
using System.Linq;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.MySql
{
    public class PlaylistMySqlContext : MySqlContext<Playlist>, IPlaylistContext
    {
        public PlaylistMySqlContext(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Playlist> GetPlaylistsForUser(User user)
        {
            var results = Database.Query("SELECT * FROM playlist WHERE ownerId = ?",
                user.Id).ToList();

            foreach (var result in results)
            {
                yield return ModelCreator<Playlist>.CreateModel(result);
            }
        }

        public IEnumerable<PlaylistItem> GetPlaylistItems(Playlist playlist)
        {
            throw new System.NotImplementedException();
        }
    }
}