using System.Collections.Generic;
using System.Linq;
using Watchtogether.DAL.Interface;
using Watchtogether.DAL.MySql.Utils;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.MySql
{
    public class UserMySqlContext : MySqlContext<User>, IUserContext
    {
        public UserMySqlContext(string connectionString) : base(connectionString)
        {
        }

        public User FindByEmail(string email)
        {
            var results = Database.Query($"SELECT * FROM {typeof(User).Name.ToLower()} WHERE email = ?",
                email).ToList();
            if (!results.Any())
            {
                return null;
            }

            var result = results.First();
            return ModelCreator<User>.CreateModel(result);
        }

        public IEnumerable<Group> GetGroups(User user)
        {
            var results = Database.Query(
                "SELECT * FROM `group` WHERE id = (SELECT groupId FROM `user_group` WHERE userId = ?)",
                user.Id);
            foreach (var result in results)
            {
                yield return ModelCreator<Group>.CreateModel(result);
            }
        }

        public IEnumerable<Room> GetAccessibleRooms(User user)
        {
            var results =
                Database.Query(
                    "SELECT room.* FROM room INNER JOIN group_room on room.id = group_room.roomId INNER JOIN user_group on user_group.userId = ?",
                    user.Id);

            foreach (var result in results)
            {
                yield return ModelCreator<Room>.CreateModel(result);
            }
        }

        public User FindByUsername(string username)
        {
            var results = Database.Query($"SELECT * FROM {typeof(User).Name.ToLower()} WHERE username = ?",
                username).ToList();
            if (!results.Any())
            {
                return null;
            }

            var result = results.First();
            return ModelCreator<User>.CreateModel(result);
        }
    }
}