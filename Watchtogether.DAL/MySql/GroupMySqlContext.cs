using System;
using System.Collections.Generic;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.MySql
{
    public class GroupMySqlContext : MySqlContext<Group>, IGroupContext
    {
        public GroupMySqlContext(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Group> GetGroupsOfUser(User user)
        {
            var results =
                Database.Query(
                    "select * from `group` where `ownerId` = ?",
                    user.Id);

            foreach (var result in results)
            {
                yield return ModelCreator<Group>.CreateModel(result);
            }
        }

        public IEnumerable<Group> GetUserGroups(User user)
        {
            var results =
                Database.Query(
                    "select `group`.* from `group` INNER JOIN user_group ug on `group`.id = ug.groupId WHERE ug.userId = ?",
                    user.Id);

            foreach (var result in results)
            {
                yield return ModelCreator<Group>.CreateModel(result);
            }
        }

        public IEnumerable<User> GetUsersFromGroup(int id)
        {
            var results =
                Database.Query("select * from `user` INNER JOIN user_group ug on user.id = ug.userId WHERE ug.groupId = ?", id);

            foreach (var result in results)
            {
                yield return ModelCreator<User>.CreateModel(result);
            }
        }

        public bool AddUser(Group @group, User user)
        {
            Database.NonQuery("INSERT INTO `user_group` (groupId, userId) VALUES (?, ?)", group.Id, user.Id);
            return true;
        }

        public bool RemoveUser(Group @group, User user)
        {
            Database.NonQuery("DELETE FROM `user_group` WHERE groupId = ? AND userId = ?", group.Id, user.Id);
            return true;
        }
    }
}