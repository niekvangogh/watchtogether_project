using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.Memory
{
    public class RoomMemoryContext : MemoryContext<Room>, IRoomContext
    {
        private static readonly Dictionary<Room, List<Group>> RoomGroups = new Dictionary<Room, List<Group>>();

        public IEnumerable<Room> GetRoomsFromUser(User user)
        {
            return Items.FindAll(room => room.OwnerId == user.Id);
        }

        public IEnumerable<Room> GetAccessibleRoomsForGroup(Group group)
        {
            return RoomGroups.Where(pair => pair.Value.Contains(@group)).Select(pair => pair.Key);
        }

        public void AddGroup(Room room, Group @group)
        {
            RoomGroups[room].Add(group);
        }

        public void RemoveGroup(Room room, Group @group)
        {
            RoomGroups[room].Remove(group);
        }

        public IEnumerable<Group> GetGroups(Room room)
        {
            return RoomGroups.Where(pair => pair.Key.Id == room.Id).SelectMany(pair => pair.Value);
        }

        public IEnumerable<Room> GetAllAccessibleRooms(User user)
        {
            return null;
        }
    }
}