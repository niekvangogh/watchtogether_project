using System.Collections.Generic;
using System.Linq;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.Memory
{
    public class GroupMemoryContext : MemoryContext<Group>, IGroupContext
    {
        private static readonly Dictionary<Group, List<User>> GroupUsers = new Dictionary<Group, List<User>>();

        public IEnumerable<Group> GetGroupsOfUser(User user)
        {
            return Items.Where(group => group.OwnerId == user.Id);
        }

        public IEnumerable<Group> GetUserGroups(User user)
        {
            return GroupUsers.Where(pair => pair.Value.Contains(user)).Select(pair => pair.Key);
        }

        public IEnumerable<User> GetUsersFromGroup(int id)
        {
            return GroupUsers.Where(pair => pair.Key.OwnerId == id).SelectMany(pair => pair.Value);
        }

        public bool AddUser(Group group, User user)
        {
            GroupUsers.First(pair => pair.Key.Id == @group.Id).Value.Add(user);
            return true;
        }

        public bool RemoveUser(Group group, User user)
        {
            GroupUsers.First(pair => pair.Key.Id == group.Id).Value.Remove(user);
            return true;
        }
    }
}