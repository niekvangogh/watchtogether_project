using System.Collections.Generic;
using System.Linq;
using Watchtogether.DAL.Interface;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.Memory
{
    public class UserMemoryContext : MemoryContext<User>, IUserContext
    {
        public User FindByEmail(string email)
        {
            var found = Items.Where(user => user.Email == email).ToList();
            return found.Any() ? found[0] : null;
        }

        public IEnumerable<Group> GetGroups(User user)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Room> GetAccessibleRooms(User user)
        {
            throw new System.NotImplementedException();
        }

        public User FindByUsername(string username)
        {
            var found = Items.Where(user => user.Username == username).ToList();
            return found.Any() ? found[0] : null;
        }
    }
}