using System.Collections.Generic;
using System.Linq;
using Watchtogether.DAL.Interface;
using Watchtogether.Model;

namespace Watchtogether.DAL.Memory
{
    public class MemoryContext<T> : IContext<T> where T : DbModel
    {
        protected MemoryContext()
        {
            Items = new List<T>();
        }

        protected static List<T> Items { get; set; }

        public T Create(T entity)
        {
            Items.Add(entity);
            entity.Id = Items.IndexOf(entity);
            return entity;
        }

        public T Read(int id)
        {
            return Items.First(type => type.Id == id);
        }

        public T Update(T entity)
        {
            var index = Items.FindIndex(type => type.Id == entity.Id);
            Items[index] = entity;
            return entity;
        }

        public bool Delete(int id)
        {
            Items.Remove(Items.First(type => type.Id == id));
            return true;
        }
    }
}