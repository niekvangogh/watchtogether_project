using System;

namespace Watchtogether.DAL
{
    public static class ModelCreator<T>
    {
        public static T CreateModel(QueryResult properties)
        {
            var model = Activator.CreateInstance<T>();
            var modelProperties = typeof(T).GetProperties();

            foreach (var property in properties.Properties)
            {
                foreach (var modelProperty in modelProperties)
                {
                    if (modelProperty.Name.ToLower().Equals(property.Key.ToLower()))
                    {
                        modelProperty.SetValue(model, property.Value);
                        break;
                    }
                }
            }

            return model;
        }
    }
}