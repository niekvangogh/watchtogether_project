using System.Collections.Generic;
using System.Linq;
using Watchtogether.Exceptions;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository;
using Watchtogether.Repository.Interface;

namespace Watchtogether.Logic
{
    public class GroupLogic : IGroupLogic
    {
        private readonly IGroupRepository _repository;

        public GroupLogic(IGroupRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Group> GetAll()
        {
            return _repository.GetAll();
        }

        public Group Create(Group entity)
        {
            return _repository.Create(entity);
        }

        public Group Read(int id)
        {
            return _repository.FindById(id);
        }

        public Group Update(Group entity)
        {
            return _repository.Update(entity);
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public IEnumerable<Group> GetGroupsOfUser(User user)
        {
            return _repository.GetGroupsOfUser(user);
        }

        public IEnumerable<Group> GetUserGroups(User user)
        {
            return _repository.GetUserGroups(user);
        }

        public IEnumerable<User> GetUsersFromGroup(int id)
        {
            return _repository.GetUsersFromGroup(id);
        }

        public bool AddUser(Group group, User user)
        {
            if (GetUsersFromGroup(group.Id).Any(user1 => user1.Id == user.Id))
            {
                throw new UserAlreadyInGroupException();
            }

            return _repository.AddUser(group, user);
        }

        public bool RemoveUser(Group @group, User user)
        {
            if (GetUsersFromGroup(@group.Id).All(user1 => user1.Id != user.Id))
            {
                throw new UserIsNotInGroupException();
            }

            return _repository.RemoveUser(group, user);
        }
    }
}