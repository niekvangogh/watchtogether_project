using System;
using System.Collections.Generic;
using System.Linq;
using DevOne.Security.Cryptography.BCrypt;
using Watchtogether.Exceptions;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository.Interface;

namespace Watchtogether.Logic
{
    public class UserLogic : IUserLogic
    {
        private readonly IUserRepository _userRepository;

        public UserLogic(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetAll();
        }

        public User Create(User entity)
        {
            return _userRepository.Create(entity);
        }

        public User Read(int id)
        {
            return _userRepository.FindById(id);
        }

        public User Update(User entity)
        {
            return _userRepository.Update(entity);
        }

        public bool Delete(int id)
        {
            return _userRepository.Delete(id);
        }

        public User Login(string email, string password)
        {
            var user = _userRepository.FindByEmail(email);
            if (user != null)
            {
                if (BCryptHelper.CheckPassword(password, user.Hash))
                {
                    return user;
                }
            }

            throw new LoginException("Email or password is incorrect");
        }

        public bool ChangeEmail(string oldEmail, string newEmail)
        {
            var user = _userRepository.FindByEmail(oldEmail);
            user.Email = newEmail;
            _userRepository.Update(user);
            return false;
        }

        public User FindByEmail(string userMail)
        {
            return _userRepository.FindByEmail(userMail);
        }

        public User FindByUsername(string username)
        {
            var user = _userRepository.FindByUsername(username);
            if (user == null)
            {
                throw new NoUserFoundException("username");
            }

            return user;
        }

        public User Register(string username, string password, string email, DateTime birthday)
        {
            if (_userRepository.FindByUsername(username) != null || _userRepository.FindByEmail(email) != null)
            {
                throw new RegisterException("Username or email already exists");
            }
            else
            {
                var hash = BCryptHelper.HashPassword(password, BCryptHelper.GenerateSalt());
                var user = new User
                {
                    Username = username,
                    Email = email,
                    Birthday = birthday,
                    Hash = hash
                };
                return _userRepository.Create(user);
            }
        }
    }
}