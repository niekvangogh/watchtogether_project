using System;
using System.Collections.Generic;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository.Interface;

namespace Watchtogether.Logic
{
    public class PlaylistLogic : IPlaylistLogic
    {
        private readonly IPlaylistRepository _playlistRepository;

        public PlaylistLogic(IPlaylistRepository playlistRepository)
        {
            _playlistRepository = playlistRepository;
        }

        public IEnumerable<Playlist> GetAll()
        {
            throw new NotImplementedException();
        }

        public Playlist Create(Playlist entity)
        {
            return _playlistRepository.Create(entity);
        }

        public Playlist Read(int id)
        {
            return _playlistRepository.FindById(id);
        }

        public Playlist Update(Playlist entity)
        {
            return _playlistRepository.Update(entity);
        }

        public bool Delete(int id)
        {
            return _playlistRepository.Delete(id);
        }

        public IEnumerable<Playlist> GetPlaylistsForUser(User user)
        {
            return _playlistRepository.GetPlaylistsForUser(user);
        }

        public IEnumerable<PlaylistItem> GetPlaylistItems(Playlist playlist)
        {
            return _playlistRepository.GetPlaylistItems(playlist);
        }
    }
}