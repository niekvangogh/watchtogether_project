using System.Collections.Generic;
using System.Linq;
using Watchtogether.Exceptions;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository;

namespace Watchtogether.Logic
{
    public class RoomLogic : IRoomLogic
    {
        private readonly RoomRepository _repository;

        public RoomLogic(RoomRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Room> GetAllAccessibleRooms()
        {
            return _repository.GetAll();
        }

        public Room Create(Room entity)
        {
            return _repository.Create(entity);
        }

        public Room Read(int id)
        {
            return _repository.FindById(id);
        }

        public Room Update(Room entity)
        {
            return _repository.Update(entity);
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }


        public IEnumerable<Room> GetRoomsFromUser(User user)
        {
            return _repository.GetRoomsFromUser(user);
        }

        public IEnumerable<Room> GetAllAccessibleRooms(User user)
        {
            return _repository.GetAllAccessibleRooms(user);
        }

        public void AddGroup(Room room, Group @group)
        {
            if (GetGroups(room).Any(group1 => group1.Id == group.Id))
            {
                throw new GroupAlreadyInRoomException();
            }

            _repository.AddGroup(room, group);
        }

        public IEnumerable<Room> GetAccessibleRoomsForGroup(Group @group)
        {
            return _repository.GetAccessibleRoomsForGroup(group);
        }

        public void RemoveGroup(Room room, Group @group)
        {
            if (!GetGroups(room).Any(group1 => group1.Id == @group.Id))
            {
                throw new GroupNotInRoomException();
            }

            _repository.RemoveGroup(room, @group);
        }

        public IEnumerable<Group> GetGroups(Room room)
        {
            return _repository.GetGroups(room);
        }
    }
}