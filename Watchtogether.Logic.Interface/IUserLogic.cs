using System;
using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.Logic.Interface
{
    public interface IUserLogic : ILogic<User>
    {
        User Login(string email, string password);

        bool ChangeEmail(string dtoOldEmail, string dtoNewEmail);

        User FindByEmail(string userMail);
        
        User FindByUsername(string username);
        
        User Register(string dtoUsername, string dtoPassword, string dtoEmail, DateTime dtoBirthday);
    }
}