using System.Collections.Generic;

namespace Watchtogether.Logic.Interface
{
    public interface ILogic<T> where T : class
    {
        T Create(T entity);

        T Read(int id);

        T Update(T entity);

        bool Delete(int id);
    }
}