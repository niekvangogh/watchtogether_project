using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.Logic.Interface
{
    public interface IPlaylistLogic : ILogic<Playlist>
    {
        IEnumerable<Playlist> GetPlaylistsForUser(User user);
        IEnumerable<PlaylistItem> GetPlaylistItems(Playlist playlist);
    }
}