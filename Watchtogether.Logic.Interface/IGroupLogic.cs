using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.Logic.Interface
{
    public interface IGroupLogic : ILogic<Group>
    {
       IEnumerable<Group> GetGroupsOfUser(User user);

       IEnumerable<Group> GetUserGroups(User user);

       IEnumerable<User> GetUsersFromGroup(int id);
       
       bool AddUser(Group group, User user);

       bool RemoveUser(Group group, User user);

    }
}