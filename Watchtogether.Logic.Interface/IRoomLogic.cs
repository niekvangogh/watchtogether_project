using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.Logic.Interface
{
    public interface IRoomLogic : ILogic<Room>
    {
        IEnumerable<Room> GetRoomsFromUser(User user);

        IEnumerable<Room> GetAllAccessibleRooms(User user);

        void AddGroup(Room room, Group @group);

        void RemoveGroup(Room room, Group @group);

        IEnumerable<Group> GetGroups(Room room);
    }
}