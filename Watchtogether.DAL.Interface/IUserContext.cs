using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.Interface
{
    public interface IUserContext : IContext<User>
    {
        User FindByEmail(string email);

        IEnumerable<Group> GetGroups(User user);
 
        IEnumerable<Room> GetAccessibleRooms(User user);
        
        User FindByUsername(string username);
    }
}