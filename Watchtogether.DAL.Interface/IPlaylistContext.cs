using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.Interface
{
    public interface IPlaylistContext : IContext<Playlist>
    {
        IEnumerable<Playlist> GetPlaylistsForUser(User user);
        IEnumerable<PlaylistItem> GetPlaylistItems(Playlist playlist);
    }
}