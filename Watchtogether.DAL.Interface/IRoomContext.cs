using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.Interface
{
    public interface IRoomContext : IContext<Room>
    {
        IEnumerable<Room> GetRoomsFromUser(User user);
        
        IEnumerable<Room> GetAccessibleRoomsForGroup(Group group);
        
        void AddGroup(Room room, Group @group);
        
        void RemoveGroup(Room room, Group @group);
        
        IEnumerable<Group> GetGroups(Room room);
        
        IEnumerable<Room> GetAllAccessibleRooms(User user);
    }
}