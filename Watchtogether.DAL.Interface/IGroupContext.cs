using System.Collections.Generic;
using Watchtogether.Model.Models;

namespace Watchtogether.DAL.Interface
{
    public interface IGroupContext : IContext<Group>
    {
        IEnumerable<Group> GetGroupsOfUser(User user);

        IEnumerable<Group> GetUserGroups(User user);
        
        IEnumerable<User> GetUsersFromGroup(int id);
        
        bool AddUser(Group group, User user);

        bool RemoveUser(Group group, User user);
    }
}