using Watchtogether.Model;

namespace Watchtogether.DAL.Interface
{
    public interface IContext<T> where T : DbModel
    {
        T Create(T entity);

        T Read(int id);

        T Update(T entity);

        bool Delete(int id);
    }
}