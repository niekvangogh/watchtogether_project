FROM microsoft/dotnet:2.2-sdk

EXPOSE 80

COPY . watchtogether/

WORKDIR watchtogether

RUN dotnet publish -c Release Watchtogether

WORKDIR Watchtogether/bin/Release/netcoreapp2.2/publish

ENTRYPOINT ["dotnet", "Watchtogether.dll"]