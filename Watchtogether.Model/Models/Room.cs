using System;

namespace Watchtogether.Model.Models
{
    public class Room : DbModel
    {
        public int OwnerId { get; set; }
        
        public int CreatorId { get; set; }

        public string Name { get; set; }

        public bool Public { get; set; }

        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public byte[] Image { get; set; }

    }
}