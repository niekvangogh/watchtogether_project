using System;
using System.Collections.Generic;

namespace Watchtogether.Model.Models
{
    public class Playlist : DbModel
    {
        public int OwnerId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreationDate { get; set; }
        
        public int CategoryId { get; set; }
    }
}