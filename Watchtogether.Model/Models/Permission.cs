﻿namespace Watchtogether.Model.Models
{
    public class Permission : DbModel
    {

        public string Name { get; set; }

        public string PermissionCode { get; set; }
    }
}