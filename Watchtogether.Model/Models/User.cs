using System;
using System.Collections.Generic;

namespace Watchtogether.Model.Models
{
    public class User : DbModel
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Hash { get; set; }

        public DateTime Birthday { get; set; }
    }
}