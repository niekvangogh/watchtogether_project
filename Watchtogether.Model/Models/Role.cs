﻿using System.Collections.Generic;

namespace Watchtogether.Model.Models
{
    public class Role : DbModel
    {
        public int GroupId { get; set; }

        public string Name { get; set; }

    }
}