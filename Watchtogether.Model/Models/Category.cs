namespace Watchtogether.Model.Models
{
    public class Category : DbModel
    {
        public string Name { get; set; }

        public bool IsAgeRestricted { get; set; }
    }
}