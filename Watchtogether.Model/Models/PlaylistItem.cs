namespace Watchtogether.Model.Models
{
    public class PlaylistItem : DbModel
    {
        public string VideoUrl { get; set; }

        public int PlaylistId { get; set; }

        public string VideoName { get; set; }
    }
}