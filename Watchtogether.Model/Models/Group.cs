﻿using System;
using System.Collections.Generic;

namespace Watchtogether.Model.Models
{
    public class Group : DbModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int OwnerId { get; set; }

        public int CreatorId { get; set; }

        public DateTime CreationDate { get; set; }
    }
}