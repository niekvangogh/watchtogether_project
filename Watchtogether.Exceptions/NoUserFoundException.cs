using System;

namespace Watchtogether.Exceptions
{
    
    public class NoUserFoundException : WatchtogetherException
    {
        public NoUserFoundException(string field) : base("No user was found with that " + field) {}
    }
}