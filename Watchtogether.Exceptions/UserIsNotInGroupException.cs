using System;

namespace Watchtogether.Exceptions
{
    public class UserIsNotInGroupException : WatchtogetherException
    {
        public UserIsNotInGroupException() : base("User was not found in the group")
        {
        }
    }
}