using System;
using Watchtogether.Model.Models;
using Group = System.Text.RegularExpressions.Group;

namespace Watchtogether.Exceptions
{
    public class UserAlreadyInGroupException : WatchtogetherException
    {
        public UserAlreadyInGroupException() : base("User already is in that group")
        {
        }
    }
}