namespace Watchtogether.Exceptions
{
    public class GroupNotInRoomException : WatchtogetherException
    {
        public GroupNotInRoomException() : base("The group does not exist in the room")
        {
        }
    }
}