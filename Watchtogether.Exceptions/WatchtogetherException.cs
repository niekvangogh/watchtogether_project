using System;

namespace Watchtogether.Exceptions
{
    public abstract class WatchtogetherException : Exception
    {
        public WatchtogetherException(string message) : base(message)
        {
        }
    }
}