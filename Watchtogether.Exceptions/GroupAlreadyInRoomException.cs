namespace Watchtogether.Exceptions
{
    public class GroupAlreadyInRoomException : WatchtogetherException
    {
        public GroupAlreadyInRoomException() : base("The group already is in the group")
        {
        }
    }
}