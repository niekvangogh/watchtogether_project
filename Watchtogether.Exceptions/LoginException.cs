using System;

namespace Watchtogether.Exceptions
{
    public class LoginException : WatchtogetherException
    {
        public LoginException(string message) : base(message)
        {
        }
    }
}