using System;

namespace Watchtogether.Exceptions
{
    public class RegisterException : WatchtogetherException
    {

        public RegisterException(string message) : base(message)
        {
            
        }
    }
}