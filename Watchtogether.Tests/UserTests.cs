using System;
using Watchtogether.DAL.Memory;
using Watchtogether.Exceptions;
using Watchtogether.Logic;
using Watchtogether.Logic.Interface;
using Watchtogether.Model.Models;
using Watchtogether.Repository;
using Xunit;

namespace Watchtogether.Tests
{
    public class UserTests
    {
        private readonly UserLogic _userLogic;

        public UserTests()
        {
            _userLogic = new UserLogic(new UserRepository(new UserMemoryContext()));
            _userLogic.Create(new User
            {
                Username = "Niek",
                Email = "ik@niekvangogh.nl",
                Hash = "$2a$10$KlwIhe4ETCe3rAzqVhLMm.7l4ApYB9qx8UrQcThi2/nu3JMHvplCm",
                Birthday = DateTime.Today
            });
        }

        [Fact]
        public void GetUserByEmail_GivenCorrectEmail_ShouldGiveCorrectUser()
        {
            var email = "ik@niekvangogh.nl";

            var user = _userLogic.FindByEmail(email);

            Assert.Equal(email, user.Email);
        }

        [Fact]
        public void GetUserByEmail_GivenInvalidEmail_ShouldReturnNull()
        {
            var email = "hackerman@niekvangogh.nl";

            var user = _userLogic.FindByEmail(email);

            Assert.Null(user);
        }

        [Fact]
        public void GetUserByEmail_GivenNull_ShouldReturnNull()
        {
            string email = null;

            var user = _userLogic.FindByEmail(email);

            Assert.Null(user);
        }

        [Fact]
        public void LoginUser_WithGivenCorrectFields_ShouldReturnUser()
        {
            var email = "ik@niekvangogh.nl";
            var password = "admin";

            var user = _userLogic.Login(email, password);

            Assert.Equal(email, user.Email);
        }

        [Fact]
        public void LoginUser_WithGivenWrongPassword_ShouldExpectException()
        {
            var email = "ik@niekvangogh.nl";
            var password = "LOSER";
            Assert.Throws<LoginException>(new Action(() => _userLogic.Login(email, password)));
        }
    }
}